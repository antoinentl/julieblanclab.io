
window.onload = function(){ 
    this.console.log("script chargé");

let scrollPos = 0;
const nav = document.querySelector('#header-socials');

function checkPosition() {
  let windowY = window.scrollY;
  if (windowY < scrollPos) {
    // Scrolling UP
    nav.classList.add('is-visible');
    nav.classList.remove('is-hidden');
  } else {
    if(windowY > 200){
      // Scrolling DOWN
      nav.classList.add('is-hidden');
      nav.classList.remove('is-visible');
    } 
  }
  scrollPos = windowY;
}

window.addEventListener('scroll', checkPosition);

}