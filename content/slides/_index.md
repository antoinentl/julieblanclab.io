---
title: "Slides list"
date: 2019-09-15T10:34:12+02:00
draft: false
---

<section id="slide-list">
<h1>Slides list</h1>

<ul>
    <li>
        <span class="title"><a href="/slides/20191004_sem-artec-editions.html">Du fanzine à l’encyclopédie, éditions avec et après le numérique</a> [fr]<span>
        <span class="infos">Séminaire « Dé-brancher l’œuvre », master ArTeC – October 4, 2019 – Université Paris 8</span>
    </li>
    <li>
        <span class="title"><a href="/slides/20190626_DigitalPublishingSummit.html">Print Books with Browsers, Designing books in browsers using Paged.js</a> [en]<span>
        <span class="infos">Digital Publishing Summit – May 26, 2019 – BnF, Paris</span>
    </li>
    <li>
        <span class="title"><a href="/slides/20190613_write-the-doc.html">Paged.js presentation</a> [en]</span>
        <span class="infos">Write the doc – 13 juin 2019, Paris</span>
    </li>
    <li>
        <span class="title"><a href="/slides/20190406_jdll.html">Faire des livres avec un navigateur</a> [fr]</span>
        <span class="infos">JDLL – 6 avril 2019, Lyon</span>
    </li>
    <li>
        <span class="title"><a href="/slides/20181027_crihn.html">Imprimé, <em>single source publishing</em> et technologies du web</a> [fr]</span>
        <span class="infos">CRIHN « Repenser les humanités numériques » – 27 octobre 2018, Montréal</span>
    </li>
    <li>
        <span class="title"> <a href="/slides/20180820_Lure.html">Paginer le flux</a> [fr]</span>
        <span class="infos">Rencontres de Lure – 20 août 2018, Lurs</span>
    </li>
</ul>
</section>