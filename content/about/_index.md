---
title: "About"
date: 2019-09-15T10:41:14+02:00
draft: false
---

Je suis designer graphique et chercheuse. Je prépare une thèse en&nbsp;ergonomie et design graphique au laboratoire Paragraphe (Université Paris&nbsp;8) et à EnsadLab-Paris dans le cadre de l’école de&nbsp;recherche universitaire ArTeC (Art, Technologies et&nbsp;Création). Je&nbsp;contribue au développement de Paged.js[⟡](#section-research) et participe à&nbsp;PrePostPrint[⟡](#section-research).

Mes travaux et recherches portent essentiellement sur l'étude du&nbsp;récent passage des logiciels de Publication Assistée par&nbsp;Ordinateur à&nbsp;l’utilisation de&nbsp;technologies et de&nbsp;méthodes du&nbsp;développement web (HTML5, CSS3, javascript, epub) dans les&nbsp;chaînes éditoriales et tout particulièrement dans les pratiques du design graphique. J’aborde ainsi les implications de&nbsp;la&nbsp;composition «&nbsp;automatisée&nbsp;» sur les formes sensibles.