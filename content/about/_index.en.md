---
title: "About"
date: 2019-09-15T10:41:14+02:00
draft: false
---

I am a graphic designer and researcher. I&nbsp;am&nbsp;preparing a&nbsp;PhD in&nbsp;ergonomics and design graphic at&nbsp;<em>Paragraphe</em> Laboratory (University of Paris&nbsp;8) and at EnsadLab-Paris in the framework of ArTeC university research school (Art,&nbsp;Technologies and Creation). I&nbsp;contribute to the development of&nbsp;Paged.js[⟡](#section-research) and participate in&nbsp;PrePostPrint[⟡](#section-research). 

My work and research focuses on the&nbsp;recent shift from desktop publishing software to the use of web development technologies and methods (HTML5, CSS3, javascript, epub) in&nbsp;editorial workflows and particularly in&nbsp;graphic design practices. I&nbsp;also look at the implications of&nbsp;“automated” typesetting on graphic&nbsp;design. 