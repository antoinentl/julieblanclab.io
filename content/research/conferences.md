---
title: "Conférences"
id: "conferences"
date: 2019-09-15T09:58:21+02:00
draft: false
---

- **Présentation de la publication numérique et imprimée du catalogue des sculptures de la villa romaine de Chiragan** [fr] \
Julie Blanc et Christelle Molinié, \
*Les Lundis numériques de l'INHA* (Paris, France), 13 janvier 2020.
    - [Slides](http://slides.julie-blanc.fr/20200113_INHA-chiragan.html)
    - [Vidéo](https://www.youtube.com/watch?v=-bTGBPaJR3o&list=PLsl8NWzVv6T2CQFtBOfnlA_EKLFeCFSUG&index=29&t=0s)
- **Du fanzine à l’encyclopédie, éditions avec et après le numérique** [fr] \
Séminaire *Dé-brancher l’œuvre*, master ArTeC, Université Paris 8, 4 octobre 2019.
    - [Slides](http://slides.julie-blanc.fr/20191004_sem-artec-editions.html)
- **Print Books with Browsers** [en] \
*Digital Publishing Summit*, BnF (Paris, France), 26 mai 2019.
    - [Slides](http://slides.julie-blanc.fr/20190626_DigitalPublishingSummit.html)
    - [Vidéo](https://www.youtube.com/watch?v=3SvfARdZRA4)
- **Présentation de paged.js** [fr/en] \
Présentation au meeting *Write the docs*, Mozilla (Paris, France), 14 juin 2019.
    - [Slides](http://slides.julie-blanc.fr/20190613_write-the-doc.html) [en]
- **Faire des livres avec un navigateur** [fr] \
Julie Blanc et Julien Taquet, \
Conférence aux *Journées du Logiciel Libre*, Maison pour tous (Lyon, France), 6 avril 2019.
    - [Slides](http://slides.julie-blanc.fr/20190406_jdll.html)
- **Imprimé, single source publishing et technologies du web** [fr]\
Conférence au colloque *Repenser les humanités numériques*, CRIHN (Montréal, Canada), 27 octobre 2018.
    - [Slides](http://slides.julie-blanc.fr/20181027_crihn.html)
- **Paginer le flux** [fr]\
Présentation aux *Rencontres internationale de Lure* (Lurs, France), 20 août 2018.
    - [Slides](http://slides.julie-blanc.fr/20180820_Lure.html)
- **Design des publications scientifiques multisupports** [fr]\
Julie Blanc et Lucile Haute, \
Conférence au colloque international *ÉCRiDiL, Écrire, éditer, lire à l’ère numérique*, Usine C (Montréal, Canada), 3 avril 2018.
- **Éditer une revue “arts et sciences” aujourd’hui**[fr]\
Avec David Bihanic, Lucile Haute, Anthony Masure, Robin de Mourat, Vincent Piccolo, Annick Rivoire, Nolwenn Tréhondart.\
Table ronde à l'évènement *Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), 2 février 2018. 
- **PagedMedia approaches** [en]\
Présentation au *Paged Media Meeting*, MIT press (Cambridge, États-Unis), 9 janvier 2018.
- **(Re)Penser les chaînes de publication : soutenabilité et émancipation** [fr]\
Julie blanc et Antoine Fauchié, \
Conférence au colloque international *Les écologies du numérique*, ÉSAD Orléans – Écolab (Orléans, France), 10 novembre 2017.
- **Standards du web et publication académique** [fr]
Julie Blanc et Lucile Haute, \
Conférence à *PrePostPrint @ Gaîté Lyrique*, Paris, 21 octobre 2018.
