---
title: "Publications"
id: "publications"
date: 2019-09-15T09:46:35+02:00
draft: false
---



- **“Timeline of technologies for publishing”** [fr]\
Julie Blanc and Lucile Haute, *Sciences du design*, 2018 /2 (n° 8), pp. 11-17.
    - [online](http://recherche.julie-blanc.fr/timeline-publishing/), english augmented version
    - on [Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-11.htm)
- **“Publier la recherche en design : (hors-)normes, (contre-)formats, (anti-)standards”** [fr] \
Julie Blanc and Lucile Haute, *Réel | Virtuel: enjeux du numérique*, n° 6, “Les normes du numériques”, 2018.
    - on [Réel-Virtuel](http://www.reel-virtuel.com/numeros/numero6/sentinelles/publier-recherche-design)
- **“Web standards and academic publishing”** [fr]\
Julie Blanc et Lucile Haute, *Code X – 01: PrePostPrint*, éditions HYX, 2017.
