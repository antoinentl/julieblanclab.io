---
title: "Organization"
id: "organization"
date: 2019-09-15T10:18:55+02:00
draft: false
---



- **EnsadLab invite PrePostPrint : Enjeux des systèmes de publication libres et outils alternatifs pour la création graphique** [fr]\
Colloquium, EnsAD (Paris), April 3–4, 2018. 
    - [Présentation et vidéos](http://www.ensadlab.fr/fr/francais-prepostprint/)
- **Éditer une revue “arts et sciences” aujourd’hui**[fr]\
Panel discussion at the event *Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), February 2, 2018.\
With David Bihanic, Lucile Haute, Anthony Masure, Robin de Mourat, Vincent Piccolo, Annick Rivoire, Nolwenn Tréhondart.
- **PrePostPrint @ Gaîté Lyrique** [fr] \ 
 Conferences and publishing exhibition, Gaîté Lyrique (Paris), October 20–21, 2017. 
