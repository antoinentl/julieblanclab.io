---
title: "Conferences"
id: "conferences"
date: 2019-09-15T09:58:21+02:00
draft: false
---

- **Présentation de la publication numérique et imprimée du catalogue des sculptures de la villa romaine de Chiragan** [fr] \
Julie Blanc et Christelle Molinié, \
*Les Lundis numériques de l'INHA* (Paris, France), January 13, 2020.
    - [Slides](http://slides.julie-blanc.fr/20200113_INHA-chiragan.html)
    - [Video](https://www.youtube.com/watch?v=-bTGBPaJR3o&list=PLsl8NWzVv6T2CQFtBOfnlA_EKLFeCFSUG&index=29&t=0s
- **Du fanzine à l’encyclopédie, éditions avec et après le numérique** [fr] \
Seminar *Dé-brancher l’œuvre*, master ArTeC, Université Paris 8, October 4, 2019.
    - [Slides](http://slides.julie-blanc.fr/20191004_sem-artec-editions.html)
- **Print Books with Browsers** [en] \
*Digital Publishing Summit*, BnF (Paris, France), May 26, 2019.
    - [Slides](http://slides.julie-blanc.fr/20190626_DigitalPublishingSummit.html)
    - [Vidéo](https://www.youtube.com/watch?v=3SvfARdZRA4)
- **Paged.js presentation** [fr/en] \
Presentation at the meeting *Write the docs*, Mozilla (Paris, France), June 14, 2019.
    - [Slides](http://slides.julie-blanc.fr/20190613_write-the-doc.html) [en]
- **Faire des livres avec un navigateur** [fr] \
Julie Blanc and Julien Taquet, \
Conference at *Les Journées du Logiciel Libre*, Maison pour tous (Lyon, France), April 6, 2019.
    - [Slides](http://slides.julie-blanc.fr/20190406_jdll.html)
- **Imprimé, single source publishing et technologies du web** [fr]\
Conference at colloquium *Repenser les humanités numériques*, CRIHN (Montréal, Canada), Octobre 27, 2018.
    - [Slides](http://slides.julie-blanc.fr/20181027_crihn.html)
- **Paginer le flux** [fr]\
Presentation at *Rencontres internationale de Lure* (Lurs, France), August 20, 2018.
    - [Slides](http://slides.julie-blanc.fr/20180820_Lure.html)
- **Design des publications scientifiques multisupports** [fr]\
Julie Blanc et Lucile Haute, \
Conference at the international colloquium *ÉCRiDiL, Écrire, éditer, lire à l’ère numérique*, Usine C (Montréal, Canada), April 30, 2018.
- **Éditer une revue “arts et sciences” aujourd’hui**[fr]\
With David Bihanic, Lucile Haute, Anthony Masure, Robin de Mourat, Vincent Piccolo, Annick Rivoire, Nolwenn Tréhondart.\
Panel discussion at the event *Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), February 2, 2018. 
- **PagedMedia approaches** [en]\
Presentation at *Paged Media Meeting*, MIT press (Cambridge, États-Unis), January 9, 2018.
- **(Re)Penser les chaînes de publication : soutenabilité et émancipation** [fr]\
Julie Blanc and Antoine Fauchié, \
Conference at the international colloquium *Les écologies du numérique*, ÉSAD Orléans – Écolab (Orléans, France), November 10, 2017.
- **Standards du web et publication académique** [fr]
Julie Blanc and Lucile Haute, \
Conference at *PrePostPrint* @ Gaîté Lyrique, Paris, October 21, 2018.
