---
title: "Publications"
id: "publications"
date: 2019-09-15T09:46:35+02:00
draft: false
---



- **« Technologies de l’édition numérique »** [fr]\
Julie Blanc et Lucile Haute, *Sciences du design*, 2018 /2 (n° 8), pp. 11-17.
    - [en ligne](http://recherche.julie-blanc.fr/timeline-publishing/), version augmentée en anglais
    - sur [Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-11.htm)
- **« Publier la recherche en design : (hors-)normes, (contre-)formats, (anti-)standards »** [fr] \
Julie Blanc et Lucile Haute, *Réel | Virtuel : enjeux du numérique*, n° 6, « Les normes du numériques », 2018.
    - sur [Réel-Virtuel](http://www.reel-virtuel.com/numeros/numero6/sentinelles/publier-recherche-design)
- **« Standards du web et publication académique »** [fr]\
Julie Blanc et Lucile Haute, *Code X – 01 : PrePostPrint*, éditions HYX, 2017.
