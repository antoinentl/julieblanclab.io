---
title: "Blogposts"
id: "blogposts"
date: 2019-09-15T10:31:24+02:00
draft: false
---

- **“Return from Prague: the future of web to print”**, <em>pagedjs.org</em>, 18&nbsp;février&nbsp;2020.
    - [https://www.pagedjs.org/posts/2020-02-18-return-from-prague-the-future-of-web-to-print/](https://www.pagedjs.org/posts/2020-02-18-return-from-prague-the-future-of-web-to-print/)
- **“Paged Media approaches: page floats”**, <em>pagedjs.org</em>, 2&nbsp;avril&nbsp;2018.
    - [https://www.pagedjs.org/page-floats/](https://www.pagedjs.org/page-floats/)
- **“What is the Paged Media initiative”**, <em>www.pagedmedia.org</em>, 5&nbsp;juin&nbsp;2018.
    - [https://www.pagedmedia.org/what-is-the-paged-media-initiative/](https://web.archive.org/web/20190506055520/https://www.pagedmedia.org/what-is-the-paged-media-initiative/)
- **“Paged Media approaches (Part 2 of 2)”**, <em>www.pagedmedia.org</em>, 13&nbsp;février&nbsp;2018.
    - [https://www.pagedmedia.org/paged-media-approaches-part-2-of-2/](https://web.archive.org/web/20190506055604/https://www.pagedmedia.org/paged-media-approaches-part-2-of-2/)
- **“Paged Media approaches (Part 1 of 2)”**, <em>www.pagedmedia.org</em>, 26&nbsp;janvier&nbsp;2018.
    - [https://www.pagedmedia.org/paged-media-approaches-part-1-of-2/](https://web.archive.org/web/20190506055444/https://www.pagedmedia.org/paged-media-approaches-part-1-of-2/)
