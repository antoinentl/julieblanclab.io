---
title: "Research Environments"
id: "research-environments"
date: 2019-09-15T09:33:51+02:00
draft: false
---

- **Paragraphe, University Paris 8** \
Laboratory of University Paris 8 Vincennes – Saint-Denis, researsh team C3U (Conception, Création, Compétences, Usages) 
- **EnsadLab–PSL** \
Research Laboratory of *École nationale supérieure des Arts Décoratifs*, group Reflective Interaction, axis Hybrid Publishing 
- **Paged.js (Cabbage Tree Labs)** \
Open-source javascript library to paginate HTML/CSS in the browser, and to apply PagedMedia controls to paginated content for the purposes of exporting print-ready, or display-friendly, PDF from the browser
    - [pagedjs.org](https://www.pagedjs.org/)
- **PrePostPrint** \
Group of people who meets at events for discuss on alternative free publishing,  particularly interested in the creation of hybrid and printed publications with web technology
    - [prepostprint.org](https://prepostprint.org/)
- **Design en Recherche** \
Network of Ph.D students and young Ph.D whose research focuses on emerging design study and practice
 - [designenrecherche.org](http://designenrecherche.org/)
