---
title: "Environnements de recherche"
id: "research-environments"
date: 2019-09-15T09:33:51+02:00
draft: false
---

- **Paragraphe, Université Paris 8** \
Laboratoire de l'université Paris 8 Vincennes – Saint-Denis, équipe C3U (Conception, Création, Compétences, Usages) 
- **EnsadLab–PSL** \
Laboratoire de recherche de l'École nationale supérieure des Arts Décoratifs, groupe Reflective Interaction, axe Hybrid Publishing 
- **Paged.js (Cabbage Tree Labs)** \
Librairie JavaScript open-source pour permettre l’export de PDF et l’affichage paginé dans le(s) navigateur(s) à partir d’HTML et CSS
    - [pagedjs.org](https://www.pagedjs.org/)
- **PrePostPrint** \
Groupe de personnes qui cultive un intérêt pour les procédés de création graphique et les systèmes de publication libres considérés comme « alternatifs » ou « non conventionnels », particulièrement s'ils sont conçus avec les technologies du web. 
    - [prepostprint.org](https://prepostprint.org/)
- **Design en Recherche** \
Réseau de doctorant-e-s et jeunes docteur-e-s dont les recherches portent sur les thématiques émergentes d’étude et de pratique du design
 - [designenrecherche.org](http://designenrecherche.org/)