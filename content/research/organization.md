---
title: "Organisation"
id: "organization"
date: 2019-09-15T10:18:55+02:00
draft: false
---



- **EnsadLab invite PrePostPrint : Enjeux des systèmes de publication libres et outils alternatifs pour la création graphique** [fr] \
Colloque, EnsAD (Paris), 3–4 avril 2018. 
    - [Présentation et vidéos](http://www.ensadlab.fr/fr/francais-prepostprint/)
- **Éditer une revue “arts et sciences” aujourd’hui**[fr] \
Table ronde à l'évènement *Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), 2 février 2018. \
Avec David Bihanic, Lucile Haute, Anthony Masure, Robin de Mourat, Vincent Piccolo, Annick Rivoire, Nolwenn Tréhondart.
- **PrePostPrint @ Gaîté Lyrique** [fr] \ 
Conférences et salon de l’édition, Gaîté Lyrique (Paris), 20–21 octobre 2017.
