---
title: "Ateliers"
id: "workshops"
date: 2019-09-15T10:23:27+02:00
draft: false
---

- **Scripter Gutenberg: des publication de papier et d'écran** \
Grands Ateliers, ÉSAD Orléans (France), 21-27 janvier 2020.
    - [site web](http://workshops.julie-blanc.fr/2020-esad-orleans/) [fr]
- **Markdown et Bibliographie** \
Design en Recherche, CRI (Centre de Recherche Interdisciplinaire), Paris (France), 18 juin 2019.
- **Création hybride avec des outils numériques libres** \
*Les écrits du numérique #4*, Alphabetville et la Marelle, Friche La Belle de Mai, Marseille (France), 23 mars 2019.
- **Paged Media × PrePostPrint workshops** \
Open Source Publishing lab, Bruxelles (Belgique), 28 novembre 2018. \
École nationale supérieure des Arts Décoratifs, Paris (France), 29 et 30 novembre 2018.
    - [article](https://web.archive.org/web/20190506055446/https://www.pagedmedia.org/an-overview-of-the-paged-media-x-prepostprint-workshop/) [en]
- **Module de recherche création « Libre Hybridation »** \
École nationale supérieure des Arts Décoratifs, Paris (France), 2018, 2019.
    - [site web 2019](http://workshops.julie-blanc.fr/libre-hybridation-2019/) [fr]
- **Écrire et  mettre en forme : quels formats, quels outils ?** \
Design en Recherche, École nationale supérieure des Arts Décoratifs, Paris (France), 5 juillet 2018.
