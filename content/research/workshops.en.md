---
title: "Workshops"
id: "workshops"
date: 2019-09-15T10:23:27+02:00
draft: false
---

- **Scripter Gutenberg: des publication de papier et d'écran** \
    Grands Ateliers, ÉSAD Orléans (France), January 21-27, 2020.
    - [website](http://workshops.julie-blanc.fr/2020-esad-orleans/) [fr]
- **Markdown and Bibliography**  \
Design en Recherche, CRI (Centre de Recherche Interdisciplinaire), Paris (France), June 18, 2019. 
- **Création hybride avec des outils numériques libres** \
*Les écrits du numérique #4*, Alphabetville and la Marelle, Friche La Belle de Mai, Marseille (France), March 23, 2019.
- **Paged Media × PrePostPrint workshops** \
Open Source Publishing lab, Brussels (Belgium), November 28, 2018. \
École nationale supérieure des Arts Décoratifs, Paris (France), November 29 & 30, 2018.
    - [blogpost](https://web.archive.org/web/20190506055446/https://www.pagedmedia.org/an-overview-of-the-paged-media-x-prepostprint-workshop/) [en]
- **Creation research module “Open Hybridization”** \
École nationale supérieure des Arts Décoratifs, Paris (France), 2018, 2019.
    - [website 2019](http://workshops.julie-blanc.fr/libre-hybridation-2019/) [fr]
- **Écrire et  mettre en forme : quels formats, quels outils ?**  \
Design en Recherche, École nationale supérieure des Arts Décoratifs, Paris (France), July 5 ,2018.
