---
title: "Villa Chiragan"
date: 2020-03-19T19:12:28+02:00
draft: false
id: "villa-chiragan"
---

Antoine Fauchié and I developed a multi-media catalogue for the Saint Raymond Museum (Toulouse, France). The catalogue presents a part of the museum's collection devoted to a group of Roman sculptures discovered at the Chiragan site in Martres-Tolosane. Inspired by the Getty Museum in Los Angeles, we designed and developed a digital version of the catalogue and a printed version. For this we proposed a workflow created specifically for the museum team and based on a set of open-source tools that we assembled (Jeckyll, Zotero, Forestry, Gitlab, paged.js, etc.).

The graphic and interactive design of the site has been entirely developed in HTML, CSS and Javascript. The printed version uses Paged.js to generate the printed catalogue directly from the web browser. The contents are entered and corrected only once by the museum to generate both versions. The catalogue is not yet printed but should be printed in the next few months.

For more information on the project, you can watch a presentation in french given by Christelle Molinié and me on January 13, 2020 at « Les Lundis numérique de l'INHA ». ([See the video on Youtube](https://www.youtube.com/watch?v=-bTGBPaJR3o&list=PLsl8NWzVv6T2CQFtBOfnlA_EKLFeCFSUG&index=29&t=0s))


- Site web: [https://villachiragan.saintraymond.toulouse.fr/](https://villachiragan.saintraymond.toulouse.fr/)
- Developed in collaboration with [Antoine Fauchié](https://www.quaternum.net/) et l’équipe du musée Saint Raymond
- October 2018 - January 2020
- Source code on [GitLab](https://gitlab.com/musee-saint-raymond/villa-chiragan)



{{< img src="images/responsive-chiragan-cover.jpg" class="border">}}
{{< img src="images/responsive-chiragan-pages.jpg" class="border">}}
<figure class="border">
<img src="images/screenshot-site-1.png">
</figure>
{{< img src="images/screenshot-site-2.png" class="border" >}}
{{< img src="images/screenshot-site-3.png" class="border" >}}
{{< img src="images/screenshot-site-4.png" class="border" >}}
{{< img src="images/print-inspector-1.png" class="border" >}}
{{< img src="images/print-inspector-2.png" class="border" >}}

<video src="images/chiragan-web.mp4" autoplay loop class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>

<video src="images/chiragan-print.mp4" autoplay loop  class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>

