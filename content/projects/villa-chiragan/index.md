---
title: "La Villa Chiragan"
date: 2020-03-19T19:12:28+02:00
draft: false
id: "villa-chiragan"
---

Pour le musée Saint Raymond (Toulouse), Antoine Fauchié et moi avons développé un catalogue multi-support. Le catalogue présente une partie de la collection du musée consacrée à une ensemble de sculptures romaines découvertes au lieu-dit Chiragan à Martres-Tolosane. Inspiré par le Getty Museum de Los Angeles, nous avons conçu et développé une version numérique du catalogue et une version imprimée. Pour cela nous avons proposé une chaîne de publication crée spécifiquement pour l'équipe du musée et basée sur un ensemble d'outils open-source que nous avons assemblés (Jeckyll, Zotero, Forestry, Gitlab, paged.js, etc.)

Le design graphique et interactif du site a entièrement été développé en HTML, CSS et Javascript. La version imprimée utilise Paged.js pour générer le catalogue imprimé directement depuis le navigateur web. Ainsi, les contenus ne sont entrés et corrigés qu'une seule fois par le musée pour générer les deux versions. Le catalogue n'est pas encore imprimé mais devrait l'être dans les prochains mois.

Pour plus d'informations sur le projet, vous pouvez regarder une conférence donnée par Christelle Molinié et moi donnée le 13 janvier 2020 aux lundis numériques de l'INHA. ([Voir la vidéo Youtube](https://www.youtube.com/watch?v=-bTGBPaJR3o&list=PLsl8NWzVv6T2CQFtBOfnlA_EKLFeCFSUG&index=29&t=0s))


- Site web: [https://villachiragan.saintraymond.toulouse.fr/](https://villachiragan.saintraymond.toulouse.fr/)
- Développé en collaboration avec [Antoine Fauchié](https://www.quaternum.net/) et l’équipe du musée Saint Raymond
- Octobre 2018 - Janvier 2020
- Code source sur [GitLab](https://gitlab.com/musee-saint-raymond/villa-chiragan)

{{< img src="images/responsive-chiragan-cover.jpg" class="border" >}}
{{< img src="images/responsive-chiragan-pages.jpg" class="border" >}}
{{< img src="images/screenshot-site-1.png" class="border" >}}
{{< img src="images/screenshot-site-2.png" class="border" >}}
{{< img src="images/screenshot-site-3.png" class="border" >}}
{{< img src="images/screenshot-site-4.png" class="border" >}}
{{< img src="images/print-inspector-1.png" class="border" >}}
{{< img src="images/print-inspector-2.png" class="border" >}}

<video src="images/chiragan-web.mp4" autoplay loop class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>

<video src="images/chiragan-print.mp4" autoplay loop  class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>

